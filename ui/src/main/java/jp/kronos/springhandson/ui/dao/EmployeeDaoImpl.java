package jp.kronos.springhandson.ui.dao;

import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriComponentsBuilder;

import jp.kronos.springhandson.ui.configuration.ApiCallConfigurationProperties;
import jp.kronos.springhandson.ui.domain.Employee;
import jp.kronos.springhandson.ui.domain.EmployeeList;

@Component
public class EmployeeDaoImpl implements EmployeeDao, InitializingBean {

	private final ApiCallConfigurationProperties properties;

	private final RestOperations restOperations;

	private String employeeApiUrlPrefix;

	public EmployeeDaoImpl(ApiCallConfigurationProperties properties, RestOperations restOperations) {
		this.properties = properties;
		this.restOperations = restOperations;
	}

	@Override
	public EmployeeList find(String name) {
		// APIコールのURL
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(this.employeeApiUrlPrefix);

		// クエリパラメータ
		if (name != null) {
			builder.queryParam("name", name);
		}

		// APIコール
		return this.restOperations.getForObject(builder.build().toUriString(), EmployeeList.class);
	}

	@Override
	public Employee get(Long id) {
		// APIコールのURL
		String getApiUrl = this.employeeApiUrlPrefix + "/{id}";
		Map<String, String> params = Collections.singletonMap("id", id.toString());

		// APIコール
		ResponseEntity<Employee> responseEntity = this.restOperations.getForEntity(getApiUrl, Employee.class, params);
		return responseEntity.getBody();
	}

	@Override
	public void add(Employee employee) {
		// APIコールのURL
		String addApiUrl = this.employeeApiUrlPrefix;

		// POSTリクエスト生成
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Employee> request = new HttpEntity<>(employee, headers);

		// APIコール
		this.restOperations.postForObject(addApiUrl, request, String.class);
	}

	@Override
	public void set(Employee employee) {
		// APIコールのURL
		String setApiUrl = this.employeeApiUrlPrefix + "/{id}";
		Map<String, String> params = Collections.singletonMap("id", employee.getId().toString());

		// POSTリクエスト生成
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Employee> request = new HttpEntity<>(employee, headers);

		// APIコール
		this.restOperations.patchForObject(setApiUrl, request, String.class, params);
	}

	@Override
	public void remove(Long id) {
		// APIコールのURL
		String removeApiUrl = this.employeeApiUrlPrefix + "/{id}";
		Map<String, String> params = Collections.singletonMap("id", id.toString());
		// APIコール
		this.restOperations.delete(removeApiUrl, params);
	}

	/**
	 * DIコンテナがBeanを生成し、プロパティがセットされた後に呼び出す
	 */
	//URLの設定
	@Override
	public void afterPropertiesSet() throws Exception {
		this.employeeApiUrlPrefix = "http://" + this.properties.getHost() + ":" + this.properties.getPort()
				+ "/api/employees";
	}
}
