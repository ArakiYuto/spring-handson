package jp.kronos.springhandson.ui.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestOperations;

@Configuration
@EnableConfigurationProperties(ApiCallConfigurationProperties.class)
public class UiConfiguration {

	private final ApiCallConfigurationProperties properties;

	public UiConfiguration(ApiCallConfigurationProperties properties) {
		this.properties = properties;
	}

	@Bean
	public RestOperations restTemplate() {
		// 通常のClientHttpRequestFactoryだとPATCHメソッドが利用できないので、HttpComponentsClientHttpRequestFactoryに置き換える
		return new RestTemplateBuilder().requestFactory(HttpComponentsClientHttpRequestFactory.class).build();
	}
}
