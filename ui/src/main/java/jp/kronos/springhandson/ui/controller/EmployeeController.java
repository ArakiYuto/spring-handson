package jp.kronos.springhandson.ui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.kronos.springhandson.ui.dao.EmployeeDao;
import jp.kronos.springhandson.ui.domain.Employee;
import jp.kronos.springhandson.ui.domain.EmployeeList;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	//下で使うDaoのインスタンスを注入
	private final EmployeeDao employeeDao;

	public EmployeeController(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	@GetMapping(path = "/home")
	public String top(Model model) {
		model.addAttribute("title", "Top Page");
		return "home";
	}

	@GetMapping(path = "/list")
	public String list(@RequestParam(required = false) String name, Model model) {
		// Employeeのfind APIをコールする
		EmployeeList employeeList = this.employeeDao.find(name);

		// Thymeleafのテンプレートに渡すデータをemployeeListというキーで登録
		// 遷移先のhtmlにデータを渡す
		model.addAttribute("employeeList", employeeList);

		// list.htmlに遷移
		return "list";
	}

	@GetMapping(path = "/add")
	public String add(Model model) {
		return "add";
	}

	@PostMapping(path = "/add")
	public String add(@ModelAttribute Employee employee, RedirectAttributes redirectAttributes) {
		// Employeeのadd APIをコールする
		this.employeeDao.add(employee);

		// リダイレクト先に連携するメッセージを格納する
		redirectAttributes.addFlashAttribute("message", "従業員を追加しました。");

		// 従業員一覧にリダイレクトする
		return "redirect:/employee/list";
	}

	@GetMapping(path = "/edit")
	public String edit(@RequestParam Long id, Model model) {
		// Employeeのfind APIをコールする
		Employee employee = this.employeeDao.get(id);

		// Thymeleafのテンプレートに渡すデータをemployeeListというキーで登録
		model.addAttribute("employee", employee);

		// edit.htmlに遷移
		return "edit";
	}

	@PostMapping(path = "/edit")
	public String edit(@ModelAttribute Employee employee, RedirectAttributes redirectAttributes) {
		// Employeeのset APIをコールする
		this.employeeDao.set(employee);
		
		// リダイレクト先に連携するメッセージを格納する
		redirectAttributes.addFlashAttribute("message", "従業員ID「" + employee.getId() + "」を更新しました。");

		// 従業員一覧にリダイレクトする
		return "redirect:/employee/list";
	}

	@PostMapping(path = "/delete")
	public String delete(@RequestParam Long id, RedirectAttributes redirectAttributes) {
		// Employeeのremove APIをコールする
		this.employeeDao.remove(id);

		// リダイレクト先に連携するメッセージを格納する
		redirectAttributes.addFlashAttribute("message", "従業員ID「" + id + "」を削除しました。");
				
		// 従業員一覧にリダイレクトする
		return "redirect:/employee/list";
	}
}
