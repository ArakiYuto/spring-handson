package jp.kronos.springhandson.ui.domain;

/**
 * 職種を表すenum値
 */
public enum JobCategory {
  /**
   * デザイナー
   */
  DESIGNER,

  /**
   * プログラマー
   */
  PROGRAMMER,

  /**
   * マネージャー
   */
  MANAGER
}
