package jp.kronos.springhandson.ui.dao;

import jp.kronos.springhandson.ui.domain.Employee;
import jp.kronos.springhandson.ui.domain.EmployeeList;

/**
 * Employeeデータにアクセスするデータオブジェクトを表すインタフェース。
 */
public interface EmployeeDao {

	/**
	 * クエリパラメータによる複数件参照処理。
	 * @param name          検索条件(名前、全後方一致)
	 * @return 条件に合致したEmployeeデータリスト
	 */
	EmployeeList find(String name);

	/**
	 * ID指定による１件参照処理。
	 *
	 * @param id リクエスト時のパスに含まれるID
	 * @return 指定されたIDのEmployeeデータ
	 */
	Employee get(Long id);

	/**
	 * Employeeの新規登録処理。
	 *
	 * @param employee 登録内容
	 */
	void add(Employee employee);

	/**
	 * ID指定による１件更新処理。
	 *
	 * @param employee 更新内容
	 */
	void set(Employee employee);

	/**
	 * ID指定による１件削除処理。
	 *
	 * @param id リクエスト時のパスに含まれるID
	 */
	void remove(Long id);
}
