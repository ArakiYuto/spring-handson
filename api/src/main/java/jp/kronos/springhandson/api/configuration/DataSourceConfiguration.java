package jp.kronos.springhandson.api.configuration;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * DataSourceをapplication.ymlの設定値を元に構成するJavaConfig実装。
 */
@Configuration
@EnableConfigurationProperties(DataSourceConfigurationProperties.class)
public class DataSourceConfiguration {

	@Autowired
	private DataSourceConfigurationProperties properties;

	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();

		// JDBCドライバ指定
		dataSource.setDriverClassName(this.properties.getDriverClassName());

		// 接続情報
		dataSource.setUrl(this.properties.getUrl());
		dataSource.setUsername(this.properties.getUsername());
		dataSource.setPassword(this.properties.getPassword());

		// コネクション数設定
		dataSource.setInitialSize(this.properties.getInitialSize());
		dataSource.setMaxIdle(this.properties.getMaxIdle());
		dataSource.setMinIdle(this.properties.getMinIdle());

		return dataSource;
	}
}
