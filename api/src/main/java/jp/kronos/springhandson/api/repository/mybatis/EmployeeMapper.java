package jp.kronos.springhandson.api.repository.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.kronos.springhandson.api.domain.Employee;

public interface EmployeeMapper {

	/**
	 * 全件検索または部分一致検索
	 * @param name
	 * @return 検索結果
	 */
	List<Employee> find(@Param("name") String name);

	/**
	 * 一件検索
	 * @param id
	 * @return 検索結果
	 */
	Employee get(@Param("id") Long id);

	/**
	 * ロック検索
	 * @param id
	 * @return 検索結果
	 */
	Employee lock(@Param("id")Long id);
	
	/**
	 * 登録
	 * @param employee
	 * @return 登録件数
	 */
	int add(Employee employee);

	/**
	 * 更新
	 * @param employee
	 * @return 更新件数
	 */
	int set(Employee employee);

	/**
	 * 削除
	 * @param employee
	 * @return 削除件数
	 */
	int remove(Employee employee);
}
