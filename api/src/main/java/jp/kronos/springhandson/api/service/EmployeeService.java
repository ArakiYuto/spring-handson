package jp.kronos.springhandson.api.service;

import jp.kronos.springhandson.api.domain.Employee;
import jp.kronos.springhandson.api.domain.EmployeeList;

/**
 * Employeeドメインオブジェクトに関する処理を行うモジュールを表すインタフェース。
 */
public interface EmployeeService {

	/**
	 * クエリパラメータによる複数件参照処理。
	 *
	 * @param selector 検索条件
	 * @return 条件に合致したEmployeeデータリスト
	 */
	EmployeeList find(String name);

	/**
	 * ID指定による１件参照処理。
	 *
	 * @param id リクエスト時のパスに含まれるID
	 * @return 指定されたIDのEmployeeデータ
	 */
	Employee get(Long id);

	/**
	 * Employeeの新規登録処理。
	 *
	 * @param employee 登録内容
	 */
	void add(Employee employee);

	/**
	 * ID指定による１件更新処理。
	 *
	 * @param employee 更新内容
	 */
	void set(Employee employee);

	/**
	 * ID指定による１件削除処理。
	 *
	 * @param id リクエスト時のパスに含まれるID
	 */
	void remove(Long id);

	/**
	 * Employeeを新規登録して、削除する。 処理としては意味ない。@Transactionalの検証用メソッド
	 *
	 * @param throwException trueの場合はadd処理後に例外をThrowしてトランザクションをロールバックする。
	 */
	void addAndRemove(Employee employee, boolean throwException) throws Exception;
}
