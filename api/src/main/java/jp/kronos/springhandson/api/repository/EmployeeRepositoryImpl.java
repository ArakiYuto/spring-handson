package jp.kronos.springhandson.api.repository;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import jp.kronos.springhandson.api.domain.Employee;
import jp.kronos.springhandson.api.domain.JobCategory;
import jp.kronos.springhandson.api.repository.mybatis.EmployeeMapper;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeRepositoryImpl.class);

	private final SqlSessionTemplate sqlSessionTemplate;

	public EmployeeRepositoryImpl(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	@Override
	public List<Employee> findList(String name) {
		return this.sqlSessionTemplate.getMapper(EmployeeMapper.class).find(name);
	}

	@Override
	public Employee findOne(Long id) {
		Employee employee = this.sqlSessionTemplate.getMapper(EmployeeMapper.class).get(id);
		if (employee == null) {
			logger.info("Employee not found. id={}", id);
			// 自作の例外 ResourceNotFoundException
			throw new ResourceNotFoundException("Employee not found");
		}
		return employee;
	}

	@Override
	public Employee lock(Long id) {
		Employee employee = this.sqlSessionTemplate.getMapper(EmployeeMapper.class).lock(id);
		if (employee == null) {
			logger.info("Employee not found. id={}", id);
			// 自作の例外 ResourceNotFoundException
			throw new ResourceNotFoundException("Employee not found");
		}
		return employee;
	}

	@Override
	public void insert(Employee employee) {
		this.sqlSessionTemplate.getMapper(EmployeeMapper.class).add(employee);
	}

	@Override
	public void update(Employee employee) {
		int result = this.sqlSessionTemplate.getMapper(EmployeeMapper.class).set(employee);
		if (result == 0) {
			logger.info("Employee not found. id={}", employee.getId());
			// 自作の例外 ResourceNotFoundException
			throw new ResourceNotFoundException("Employee not found");
		}
	}

	@Override
	public void delete(Employee employee) {
		int result = this.sqlSessionTemplate.getMapper(EmployeeMapper.class).remove(employee);
		if (result == 0) {
			logger.info("Employee not found. id={}", employee.getId());
			// 自作の例外 ResourceNotFoundException
			throw new ResourceNotFoundException("Employee not found");
		}
	}

}
