package jp.kronos.springhandson.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;

/**
 * �]�ƈ���\���h���C���I�u�W�F�N�g
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Employee {
	/**
	 * �]�ƈ�ID
	 */
	private Long id;

	/**
	 * �]�ƈ���
	 */
	private String name;

	/**
	 * �E��
	 */
	private JobCategory jobCategory;

	/**
	 * �o�^����
	 */
	@JsonIgnore
	private LocalDateTime createdOn;

	/**
	 * �X�V����
	 */
	@JsonIgnore
	private LocalDateTime updatedOn;

	/**
	 * �폜����
	 */
	@JsonIgnore
	private LocalDateTime deletedOn;

	/**
	 * �폜�t���O
	 */
	@JsonIgnore
	private Boolean deleted;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JobCategory getJobCategory() {
		return jobCategory;
	}

	public void setJobCategory(JobCategory jobCategory) {
		this.jobCategory = jobCategory;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(LocalDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	public LocalDateTime getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(LocalDateTime deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
}
