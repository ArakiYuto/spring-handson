package jp.kronos.springhandson.api.repository;

/**
 * 検索、更新、削除対象のリソースが存在しな�?ことを表すカスタ�?例�?
 */
public class ResourceNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ResourceNotFoundException(String message) {
		super(message);
	}

	public ResourceNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
