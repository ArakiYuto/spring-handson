package jp.kronos.springhandson.api.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jp.kronos.springhandson.api.domain.Employee;
import jp.kronos.springhandson.api.domain.EmployeeList;
import jp.kronos.springhandson.api.service.EmployeeService;

@RestController
@RequestMapping("/api/employees") //Get呼び出し
public class EmployeeRestController {
	private final EmployeeService service;

	public EmployeeRestController(EmployeeService service) {
		this.service = service;
	}

	/**
	 * クエリパラメータによる複数件参照処理。
	 *
	 * @param name          クエリパラメータの「name」の値
	 * @return 条件に合致したEmployeeデータリスト
	 */
	@GetMapping(path = "", produces = "application/json") 
	//produces = "application/json"←「json形式で戻り値を返す」という指定
	//required = false←必須ではないという意味
	//?name=johnnyの場合、String nameのnameに"johnny"という値が入る
	public EmployeeList find(@RequestParam(name = "name", required = false) String name) {
		return this.service.find(name);
	}

	/**
	 * ID指定による１件参照処理。
	 *
	 * @param id リクエスト時のパスに含まれるID
	 * @return 指定されたIDのEmployeeデータ
	 */
	@GetMapping(path = "/{id}", produces = "application/json")
	//@PathVariable←URLの値を受け取る
	///api/employees/1だったら、取り出されるのは"1"
	public Employee get(@PathVariable Long id) {
		return this.service.get(id);
	}

	/**
	 * Employeeの新規登録処理。
	 *
	 * @param employee 登録内容
	 */
	@PostMapping(path = "", produces = "application/json")
		//Bodyに入っているemployeeの情報を受け取る
		public void add(@RequestBody Employee employee) {
		//addメソッドを呼ぶ
		this.service.add(employee);
	}

	/**
	 * ID指定による１件更新処理。
	 *
	 * @param id       リクエスト時のパスに含まれるID
	 * @param employee 更新内容
	 */
	@PatchMapping(path = "/{id}", produces = "application/json")
	public void set(@PathVariable Long id, @RequestBody Employee employee) {
		employee.setId(id);
		this.service.set(employee);
	}

	/**
	 * ID指定による１件削除処理。
	 *
	 * @param id リクエスト時のパスに含まれるID
	 */
	@DeleteMapping(path = "/{id}", produces = "application/json")
	public void remove(@PathVariable Long id) {
		this.service.remove(id);
	}

}
