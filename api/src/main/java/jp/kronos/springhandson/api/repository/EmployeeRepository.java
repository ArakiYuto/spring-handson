package jp.kronos.springhandson.api.repository;

import java.util.List;

import jp.kronos.springhandson.api.domain.Employee;

/**
 * Employeeドメインオブジェクトをデータストアに格納、検索するモジュールを表すインタフェース。
 */
public interface EmployeeRepository {

	/**
	 * クエリパラメータによる複数件参照処理。
	 * @param name 名前
	 * @return 条件に合致したEmployeeデータリスト
	 */
	List<Employee> findList(String name);
	
	/**
	 * ID指定による１件参照処理。
	 * @param id リクエスト時のパスに含まれるID
	 * @return 指定されたIDのEmployeeデータ
	 */
	Employee findOne(Long id);

	/**
	 * ID指定による行ロック
	 * @param id リクエスト時のパスに含まれるID
	 * @return 指定されたIDのEmployeeデータ
	 */
	Employee lock(Long id);

	/**
	 * Employeeの新規登録処理。
	 * @param employee 登録内容
	 */
	void insert(Employee employee);

	/**
	 * ID指定による１件更新処理。
	 * @param employee 更新内容
	 */
	void update(Employee employee);

	/**
	 * ID指定による１件削除処理。
	 * @param employee 削除内容
	 */
	void delete(Employee employee);
	
}
