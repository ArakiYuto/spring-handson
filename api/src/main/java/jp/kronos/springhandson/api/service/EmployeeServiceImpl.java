package jp.kronos.springhandson.api.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.kronos.springhandson.api.domain.Employee;
import jp.kronos.springhandson.api.domain.EmployeeList;
import jp.kronos.springhandson.api.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository repository;

	public EmployeeServiceImpl(EmployeeRepository repository) {
		this.repository = repository;
	}

	@Override
	public EmployeeList find(String name) {
		EmployeeList employeeList = new EmployeeList();
		employeeList.setEmployees(this.repository.findList(name));
		return employeeList;
	}

	@Override
	public Employee get(Long id) {
		return this.repository.findOne(id);
	}

	@Override
	public void add(Employee employee) {
		this.repository.insert(employee);
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void set(Employee employee) {
		this.repository.lock(employee.getId());
		this.repository.update(employee);
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void remove(Long id) {
		Employee target = this.repository.lock(id);
		this.repository.delete(target);
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	//　boolean throwException←例外を意図的に投げるかどうか
	// trueで呼び出すとロールバックされる
	public void addAndRemove(Employee employee, boolean throwException) throws Exception {
		this.repository.insert(employee);
		if (throwException) {
			throw new Exception("It's a Test Rollback ");
		}
		this.repository.lock(employee.getId());
		this.repository.delete(employee);
	}
}
