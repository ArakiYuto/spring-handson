package jp.kronos.springhandson.api.controller;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import jp.kronos.springhandson.api.domain.Employee;
import jp.kronos.springhandson.api.domain.EmployeeList;
import jp.kronos.springhandson.api.domain.JobCategory;
import jp.kronos.springhandson.api.service.EmployeeService;
import jp.kronos.springhandson.api.util.UnitTestUtil;

/**
 * Controllerのテストは、 「Javaロジックの検証」 加えて、 「HTTPメソッド、Uriの埋め込みパラメータの検証」も行なう。
 * Controllerの対象メソッドの呼び出しは、MockMvcを利用する。
 */
public class EmployeeRestControllerTest {

	/**
	 * Mockの検証ポイント 1. 対象のメソッドを想定したパラメータを指定して呼び出せること。 2. 呼び出し回数が想定どおりであること。
	 */
	@Mock
	private EmployeeService employeeService;

	@InjectMocks
	private EmployeeRestController target;

	private MockMvc mockMvc;

	/**
	 * テストメソッド実行前にMockオブジェクトを作成する。 "@Mock"のフィールドにMockオブジェクトをInjectする。
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(target).setMessageConverters().build();
	}

	@Test
	public void testFind() throws Exception {
		// setup: 事前準備
		String name = null;
		EmployeeList findResult = new EmployeeList();

		// Mock呼び出し時の動作を定義
		Mockito.doReturn(findResult).when(employeeService).find(name);

		// when: 対象のAPIリクエストを実行
		// performメソッド
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/employees").param("name", name))
				// 期待値の設定
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		// then: テスト結果の検証。戻り値、Mockの呼び出し方法、回数など
		// UnitTestUtil←json形式で返ってきているかどうかの検証
		assertEquals(UnitTestUtil.entity2JsonText(findResult), result.getResponse().getContentAsString());
		Mockito.verify(employeeService, Mockito.times(1)).find(name);
	}

	@Test
	public void testGet() throws Exception {
		// setup: 事前準備
		Long id = 10L;
		Employee getResult = new Employee();

		// Mock呼び出し時の動作を定義
		Mockito.doReturn(getResult).when(employeeService).get(id);

		// when: 対象のAPIリクエストを実行
		// get("/api/employees/" + id.toString())←URLにパラメータを指定
		MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/employees/" + id.toString()))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		// then: テスト結果の検証。戻り値、Mockの呼び出し方法、回数など
		assertEquals(UnitTestUtil.entity2JsonText(getResult), result.getResponse().getContentAsString());
		Mockito.verify(employeeService, Mockito.times(1)).get(id);

	}

	@Test
	public void testAdd() throws Exception {
		// setup
		// Mock
		Mockito.doNothing().when(employeeService).add(ArgumentMatchers.any(Employee.class));

		// when
		// URLでメソッドが呼び出されているかの検証
		this.mockMvc.perform(MockMvcRequestBuilders.post("/api/employees/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(UnitTestUtil.entity2JsonText(new Employee())));
		// then: テスト結果の検証。戻り値、Mockの呼び出し方法、回数など
		Mockito.verify(employeeService, Mockito.times(1)).add(ArgumentMatchers.any(Employee.class));
	}

	@Test
	public void testSet() throws Exception {
		//setup
		Long id = 1L;
		//idの値が同じになっているかどうかの確認
		//{}内の結果が変数matcherに入る
		ArgumentMatcher<Employee> matcher = argument -> { //ラムダ式（無名関数）
			assertEquals(id, argument.getId());
			return true;
		};
		Mockito.doNothing().when(employeeService).set(Mockito.argThat(matcher));
		
		//when
		mockMvc.perform(MockMvcRequestBuilders.patch("/api/employees/" + id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(UnitTestUtil.entity2JsonText(new Employee())));
		
		//then
		Mockito.verify(employeeService, Mockito.times(1)).set(Mockito.argThat(matcher));
	}

	@Test
	public void testRemove() throws Exception {
		//setup
		Long id = 1L;
		Mockito.doNothing().when(employeeService).remove(id);
		
		//when
		mockMvc.perform(MockMvcRequestBuilders.delete("/api/employees/" + id));
		
		//then
		Mockito.verify(employeeService, Mockito.times(1)).remove(id);
	}

}
