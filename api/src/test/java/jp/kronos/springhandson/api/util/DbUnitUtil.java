package jp.kronos.springhandson.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import javax.sql.DataSource;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.XmlDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jp.kronos.springhandson.api.configuration.DataSourceConfigurationProperties;

@Component
public class DbUnitUtil {
	private static DataSourceConfigurationProperties properties;

	@Autowired
	public void setProperties(DataSourceConfigurationProperties properties) {
		DbUnitUtil.properties = properties;
	}

	/**
	 * dataFileで指定したXMLファイルを該当のテーブルに登録する。
	 */
	public static void loadData(DataSource source, File dataFile) throws Exception {
		IDatabaseConnection connection = null;

		try {
			connection = dbUnitConnection(source);
			FileInputStream inFile = new FileInputStream(dataFile);
			XmlDataSet dataSet = new XmlDataSet(inFile);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	/**
	 * tableNamesで指定したテーブルのデータをbackupファイル(XML)にバックアップする。
	 */
	public static void backup(DataSource source, File backup, String... tableNames) throws Exception {
		IDatabaseConnection connection = null;
		try {
			connection = dbUnitConnection(source);
			IDataSet partialDataSet = connection.createDataSet(tableNames);
			XmlDataSet.write(partialDataSet, new FileOutputStream(backup));
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	/**
	 * backupファイルにあるデータをDBに戻す backupファイルはデータ戻し後に削除する。
	 */
	public static void restoreBackup(DataSource source, File backup) throws Exception {
		restoreBackup(source, backup, false);
	}

	/**
	 * backupファイルにあるデータをDBに戻す
	 *
	 * @param deleteOnExit (trueの場合は、全テストケース終了後にbackupを削除する)
	 */
	public static void restoreBackup(DataSource source, File backup, boolean deleteOnExit) throws Exception {
		loadData(source, backup);
		if (deleteOnExit) {
			backup.deleteOnExit();
		} else {
			backup.delete();
		}
	}

	public static void assertMutateResult(DataSource source, String tableName, File expected, List<String> skipCols)
			throws Exception {
		IDatabaseConnection connection = null;
		try {
			connection = dbUnitConnection(source);

			ITable expectedTable = getXmlITable(expected.getPath(), tableName);
			ITable actualTable = getCurrentITable(connection, tableName);

			Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, skipCols.toArray(new String[0]));

		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	static ITable getXmlITable(String path, String tableName) throws Exception {
		XmlDataSet dataSet = new XmlDataSet(new FileInputStream(path));
		return dataSet.getTable(tableName);
	}

	static ITable getCurrentITable(IDatabaseConnection connection, String tableName) throws Exception {
		try {
			IDataSet dbDataSet = connection.createDataSet();
			return dbDataSet.getTable(tableName);
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}

	static IDatabaseConnection dbUnitConnection(DataSource dataSource) throws Exception {
		IDatabaseConnection connection = new DatabaseConnection(dataSource.getConnection(), DbUnitUtil.properties.getSchema());
		connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		connection.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());
		return connection;
	}
}
