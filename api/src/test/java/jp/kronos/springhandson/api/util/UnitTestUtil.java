package jp.kronos.springhandson.api.util;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * UnitTestのユーティリティ
 */
public class UnitTestUtil {

	/**
	 * LocalDateTimeを年月日時分秒でassertする。
	 */
	public static void assertDateTimeUntilSeconds(LocalDateTime actual, LocalDateTime expected) throws Exception {
		assertNotNull("actual is null", actual);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		assertEquals(expected.format(formatter), actual.format(formatter));
	}

	/**
	 * JSON形式の文字列に変換する。
	 */
	public static String entity2JsonText(Object entity) throws Exception {
		return new ObjectMapper().writeValueAsString(entity);
	}
}
