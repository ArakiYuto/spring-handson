package jp.kronos.springhandson.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import jp.kronos.springhandson.api.configuration.DataSourceConfigurationProperties;
import jp.kronos.springhandson.api.domain.Employee;
import jp.kronos.springhandson.api.domain.EmployeeList;
import jp.kronos.springhandson.api.domain.JobCategory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestOperations;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * アプリケーションを起動してのe2eテストケース
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiApplicationTests implements ApplicationContextAware {

	private static final RestTemplateBuilder builder = new RestTemplateBuilder()
			.additionalInterceptors();

	private static final ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

	private static final String REQUEST_HOST = "localhost";

	private static final String REQUEST_URI = "services/v1/employees";

	/**
	 * アプリケーションのConfig値を検証する際に使用する。
	 *
	 * @see #contextLoads
	 */
	private ApplicationContext applicationContext;

	@LocalServerPort
	private int port;

	/**
	 * アプリケーションのConfig値を検証する。 実行環境により変更する要素などをテストする。 例としてコネクションプールの設定値をテストする。
	 */
	@Test
	public void contextLoads() {
		DataSourceConfigurationProperties dataSourceConfig = applicationContext
				.getBean(DataSourceConfigurationProperties.class);
		assertThat(dataSourceConfig.getDriverClassName(), is("com.mysql.jdbc.Driver"));
		assertThat(dataSourceConfig.getUrl(), is("jdbc:mysql://localhost:3306/example?useSSL=false&serverTimezone=JST&currentSchema=example"));
		assertThat(dataSourceConfig.getInitialSize(), is(1));
		assertThat(dataSourceConfig.getMaxIdle(), is(3));
		assertThat(dataSourceConfig.getMinIdle(), is(1));
	}

	/**
	 * アプリケーションのテスト Employeeを1件登録して、Nameを指定して登録対象データが照会できること。
	 * CI(mvn test)で結合試験並みのテストシナリオを検証できる
	 */
	@Test
	public void testAddAndGet() throws Exception {
		Employee employee = new Employee();
		employee.setName("Spring Boot Test Employee");
		employee.setJobCategory(JobCategory.DESIGNER);
		RestOperations apiClient = getApiClient();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Employee> request = new HttpEntity<>(employee, headers);

		apiClient.exchange(getRequestUrl(port), HttpMethod.POST, request, Void.class);
		ResponseEntity<EmployeeList> responseEntity = apiClient.exchange(
				getRequestUrl(port) + "?name=" + employee.getName(), HttpMethod.GET, null, EmployeeList.class);
		assertThat(responseEntity.getBody().getEmployees().isEmpty(), is(false));

		dumpJson(responseEntity);

	}

	// 以下、テスト実施のためのUtilメソッド
	static RestOperations getApiClient() {
		return builder.build();
	}

	static void dumpJson(Object value) throws Exception {
		System.out.println(mapper.writeValueAsString(value));
	}

	static String getRequestUrl(int port) {
		return getRequestUrl(port, null);
	}

	static String getRequestUrl(int port, Long id) {
		String url = String.format("http://%s:%d/%s", REQUEST_HOST, port, REQUEST_URI);
		return id != null ? url + "/" + id : url;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
