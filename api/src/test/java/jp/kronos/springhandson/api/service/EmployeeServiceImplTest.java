package jp.kronos.springhandson.api.service;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import jp.kronos.springhandson.api.ApiApplication;
import jp.kronos.springhandson.api.configuration.DataSourceConfiguration;
import jp.kronos.springhandson.api.domain.Employee;
import jp.kronos.springhandson.api.domain.EmployeeList;
import jp.kronos.springhandson.api.domain.JobCategory;
import jp.kronos.springhandson.api.repository.EmployeeRepository;
import jp.kronos.springhandson.api.util.DbUnitUtil;

@RunWith(Enclosed.class)
public class EmployeeServiceImplTest {

	public static class UseMockitoTest {
		// @Mockを付けてMock化する
		@Mock
		private EmployeeRepository employeeRepository;

		@Before
		public void before() throws Exception {
			MockitoAnnotations.initMocks(this);
		}

		@Test
		public void testFind() throws Exception {
			// setup
			String input = null;
			// Mockが返す値を設定
			List<Employee> findResult = new ArrayList<>();
			EmployeeServiceImpl target = new EmployeeServiceImpl(this.employeeRepository);

			// Mock呼び出し時の動作を定義
			Mockito.doReturn(findResult).when(employeeRepository).findList(input);

			// when
			EmployeeList employeeList = target.find(input);

			// then
			assertEquals(findResult, employeeList.getEmployees());
			// findメソッドが一回呼ばれたかどうかの確認
			Mockito.verify(employeeRepository, Mockito.times(1)).findList(input);
		}

		@Test
		public void testGet() throws Exception {
			// setup
			Long input = 1000L;
			Employee findResult = new Employee();
			EmployeeServiceImpl target = new EmployeeServiceImpl(this.employeeRepository);

			// Mock呼び出し時の動作を定義
			Mockito.doReturn(findResult).when(employeeRepository).findOne(input);

			// when
			Employee result = target.get(input);

			// then
			assertEquals(findResult, result);
			Mockito.verify(employeeRepository, Mockito.times(1)).findOne(input);
		}

		@Test
		public void testAdd() throws Exception {
			// setup
			Employee employee = new Employee();
			EmployeeServiceImpl target = new EmployeeServiceImpl(this.employeeRepository);

			// 戻り値がないのでdoNothingを使う
			Mockito.doNothing().when(employeeRepository).insert(employee);

			// when
			target.add(employee);

			// then
			Mockito.verify(employeeRepository, Mockito.times(1)).insert(employee);
		}

		@Test
		public void testSet() throws Exception {
			// setup
			Employee employee = new Employee();
			employee.setId(1000L);
			EmployeeServiceImpl target = new EmployeeServiceImpl(this.employeeRepository);

			// 戻り値がないのでdoNothingを使う
			Mockito.doReturn(employee).when(employeeRepository).lock(employee.getId());
			Mockito.doNothing().when(employeeRepository).update(employee);

			// when
			target.set(employee);

			// then
			//　メソッドが一回ずつ呼ばれているかどうかの確認
			Mockito.verify(employeeRepository, Mockito.times(1)).lock(employee.getId());
			Mockito.verify(employeeRepository, Mockito.times(1)).update(employee);
		}

		@Test
		public void testRemove() throws Exception {
			// setup
			Long id = 1000L;
			Employee employee = new Employee();
			Mockito.doReturn(employee).when(employeeRepository).lock(id);
			Mockito.doNothing().when(employeeRepository).delete(employee);
			EmployeeServiceImpl target = new EmployeeServiceImpl(employeeRepository);
			
			// when
			target.remove(id);
			
			// then
			Mockito.verify(employeeRepository, Mockito.times(1)).lock(id);
			Mockito.verify(employeeRepository, Mockito.times(1)).delete(employee);
		}
	}

	/**
	 * Transactionalアノテーションでトランザクション管理(Commit or Rollback)ができることを実演するケース
	 * 本来業務では実装されないテストケース。
	 */
	@RunWith(SpringRunner.class)
	@SpringBootTest(classes = ApiApplication.class)
	@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionalTest.class })
	public static class TransactionalTest extends AbstractTestExecutionListener {

		static final Logger logger = LoggerFactory.getLogger(TransactionalTest.class);

		@Autowired
		EmployeeService target;

		@Autowired
		DataSourceConfiguration dataSourceConfiguration;

		private static final String DATA_DIR = new File(
				System.getProperty("user.dir") + "\\src\\test\\resources\\service") + File.separator;

		private final File BACKUP_DATA = new File(DATA_DIR + "backup.xml");
		private final File INIT_DATA = new File(DATA_DIR + "db_init.xml");
		private final File EXPECTED_DATA = new File(DATA_DIR + "db_expected.xml");

		@Override
		public void beforeTestMethod(TestContext testContext) throws Exception {
			DataSource dataSource = testContext.getApplicationContext().getBean(DataSourceConfiguration.class)
					.dataSource();
			DbUnitUtil.backup(dataSource, BACKUP_DATA, "EMPLOYEE");
			DbUnitUtil.loadData(dataSource, INIT_DATA);
		}

		@Override
		public void afterTestMethod(TestContext testContext) throws Exception {
			DataSource dataSource = testContext.getApplicationContext().getBean(DataSourceConfiguration.class)
					.dataSource();
			DbUnitUtil.restoreBackup(dataSource, BACKUP_DATA);
		}

		/**
		 * TransactionがCommitされることを実演。 新規登録したデータを削除(論理削除)する。
		 */
		@Test
		public void testCommit() throws Exception {
			// setup
			Employee employee = new Employee();
			employee.setId(6L);
			employee.setName("Johnny");
			employee.setJobCategory(JobCategory.PROGRAMMER);

			// when
			target.addAndRemove(employee, false);

			// then
			DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "EMPLOYEE", EXPECTED_DATA,
					Arrays.asList("ID", "CREATED_ON", "DELETED_ON"));
		}

		/**
		 * TransactionがRollbackされることを実演。 新規登録し、論理削除する前に例外をThrowする。
		 */
		@Test
		public void testRollback() throws Exception {
			// setup
			Employee employee = new Employee();
			employee.setId(6L);
			employee.setName("Johnny");
			employee.setJobCategory(JobCategory.PROGRAMMER);

			try {
				// when
				target.addAndRemove(employee, true);
			} catch (Exception ex) {
				// Rollbackを検証するためにExceptionを握る。
				logger.error(ex.getMessage());
			}

			// then
			// EMPLOYEEテーブルの内容は、初期ロードと同じ内容
			DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "EMPLOYEE", INIT_DATA,
					Collections.emptyList());
		}

	}

}
