package jp.kronos.springhandson.api.repository;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import jp.kronos.springhandson.api.ApiApplication;
import jp.kronos.springhandson.api.configuration.DataSourceConfiguration;
import jp.kronos.springhandson.api.domain.Employee;
import jp.kronos.springhandson.api.domain.JobCategory;
import jp.kronos.springhandson.api.util.DbUnitUtil;
import jp.kronos.springhandson.api.util.UnitTestUtil;

//おおもとのクラスの中で、SELECTをするクラスとINSERTをするクラスを生成している
//おおもとのクラスに@RunWith(Enclosed.class)をつけると、ネストクラスにできる
@RunWith(Enclosed.class)
public class EmployeeRepositoryImplDbUnitTest {

	private static final String DATA_DIR = new File(System.getProperty("user.dir") + 
			"\\src\\test\\resources\\repository") + File.separator;
	private static final File INIT_DATA = new File(DATA_DIR + "db_init.xml");

	/**
	 * SELECTを検証するテスト
	 */
	@RunWith(SpringRunner.class)
	@SpringBootTest(classes = ApiApplication.class)
	@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
			FindDbTest.FindTestExecutionListener.class })
	public static class FindDbTest extends AbstractTestExecutionListener {

		@Autowired
		private EmployeeRepository target;

		//　全件検索をするためのメソッド
		@Test
		public void testFindAll() throws Exception {
			List<Employee> employees = target.findList(null);
			
			//　とってきた値が正しいかどうか検証する
			// 削除済みを除く、4件のデータが取得される
			assertEquals(4, employees.size());
			assertEquals(Long.valueOf(1L), employees.get(0).getId());
			assertEquals(Long.valueOf(3L), employees.get(1).getId());
			assertEquals(Long.valueOf(4L), employees.get(2).getId());
			assertEquals(Long.valueOf(5L), employees.get(3).getId());
		}

		@Test
		public void testFindPart() throws Exception {
			String name = "S";

			List<Employee> employees = target.findList(name);
			assertEquals(2, employees.size());
			assertEquals(Long.valueOf(1L), employees.get(0).getId());
			assertEquals(Long.valueOf(5L), employees.get(1).getId());
		}

		@Test
		public void testGet() throws Exception {
			Employee employee = target.findOne(1L);
			assertNotNull("employee is null", employee);
			assertEquals(Long.valueOf(1L), employee.getId());
			assertEquals("Suzuki", employee.getName());
			assertEquals(JobCategory.PROGRAMMER, employee.getJobCategory());

			UnitTestUtil.assertDateTimeUntilSeconds(employee.getCreatedOn(), LocalDateTime.parse("2020-07-01T10:00:00"));
		    UnitTestUtil.assertDateTimeUntilSeconds(employee.getUpdatedOn(), LocalDateTime.parse("2020-07-02T13:00:00"));
			assertNull("deletedOn is not null", employee.getDeletedOn());
			assertEquals(false, employee.getDeleted());
		}

		@Test(expected = ResourceNotFoundException.class)
		public void testGetException() throws Exception {
			target.findOne(7L);
		}

		@Test
		public void testLock() throws Exception {
			Employee employee = target.lock(1L);
			//値の検証
			assertNotNull("employee is null", employee);
			assertEquals(Long.valueOf(1L), employee.getId());
			assertEquals("Suzuki", employee.getName());
			assertEquals(JobCategory.PROGRAMMER, employee.getJobCategory());

			UnitTestUtil.assertDateTimeUntilSeconds(employee.getCreatedOn(), LocalDateTime.parse("2020-07-01T10:00:00"));
		    UnitTestUtil.assertDateTimeUntilSeconds(employee.getUpdatedOn(), LocalDateTime.parse("2020-07-02T13:00:00"));
			//　削除されているかどうかの検証
		    assertNull("deletedOn is not null", employee.getDeletedOn());
			assertEquals(false, employee.getDeleted());
			
		}
		//ResourceNotFoundExceptionが発生するかどうかのテスト
		@Test(expected = ResourceNotFoundException.class)
		public void testLockException() throws Exception {
			//　存在しないデータを指定
			target.lock(7L);
		}

		/**
		 * テスト前処理、後処理を行なうクラス。 １．テストクラスがloadする前にDBをテスト用にセットアップする。
		 * ２．テストクラスの全ケース終了後にDBをテスト実行前の状態に戻す。
		 */
		static class FindTestExecutionListener extends AbstractTestExecutionListener {

			private static File backup;

			/**
			 * テストで利用するテーブルのバックアップを取得する。 テストで利用するデータをloadする。
			 */
			@Override
			public void beforeTestClass(TestContext testContext) throws Exception {
				DataSource source = getDataSource(testContext.getApplicationContext());
				// 今あるデータベースの値のバックアップを取る
				backup = new File(DATA_DIR + "EMPLOYEE_back.xml");
				DbUnitUtil.backup(source, backup, "EMPLOYEE");
				DbUnitUtil.loadData(source, INIT_DATA);
			}

			/**
			 * 取得したバックアップをリストアし、実行前の状態に復元する。
			 */
			@Override
			public void afterTestClass(TestContext testContext) throws Exception {
				//　バックアップのデータを戻す
				DbUnitUtil.restoreBackup(getDataSource(testContext.getApplicationContext()), backup);
			}

			private DataSource getDataSource(ApplicationContext applicationContext) throws Exception {
				return applicationContext.getBean(DataSourceConfiguration.class).dataSource();
			}
		}
	}

	/**
	 * INSERTのSQLを検証するテスト
	 */
	@RunWith(SpringRunner.class)
	@SpringBootTest(classes = ApiApplication.class)
	@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
			InsertDbTest.InsertTestExecutionListener.class })
	public static class InsertDbTest extends AbstractTestExecutionListener {

		private static final File EXPECTED_DATA = new File(DATA_DIR + "insert_db_expected.xml");

		@Autowired
		private EmployeeRepository target;

		@Autowired
		private DataSourceConfiguration dataSourceConfiguration;

		@Test
		public void testInsert() throws Exception {
			Employee employee = new Employee();
			employee.setName("Johnny");
			employee.setJobCategory(JobCategory.MANAGER);
			employee.setDeleted(false);
			target.insert(employee);
			DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "EMPLOYEE", EXPECTED_DATA,
					Arrays.asList("ID", "CREATED_ON"));
		}

		/**
		 * テスト前処理、後処理を行なうクラス。 １．テストクラスがloadする前にDBをテスト用にセットアップする。
		 * ２．テストクラスの全ケース終了後にDBをテスト実行前の状態に戻す。
		 */
		static class InsertTestExecutionListener extends AbstractTestExecutionListener {

			private static File backup;

			/**
			 * テストで利用するテーブルのバックアップを取得する。 テストで利用するデータをloadする。
			 */
			@Override
			public void beforeTestClass(TestContext testContext) throws Exception {
				DataSource source = getDataSource(testContext.getApplicationContext());
				backup = new File(DATA_DIR + "EMPLOYEE_back.xml");
				DbUnitUtil.backup(source, backup, "EMPLOYEE");
				DbUnitUtil.loadData(source, INIT_DATA);
			}

			/**
			 * 取得したバックアップをリストアし、実行前の状態に復元する。
			 */
			@Override
			public void afterTestClass(TestContext testContext) throws Exception {
				DbUnitUtil.restoreBackup(getDataSource(testContext.getApplicationContext()), backup);
			}

			private DataSource getDataSource(ApplicationContext applicationContext) throws Exception {
				return applicationContext.getBean(DataSourceConfiguration.class).dataSource();
			}
		}
	}

	/**
	 * UPDATE/REMOVE(論理削除)のSQLを検証するテスト
	 */
	@RunWith(SpringRunner.class)
	@SpringBootTest(classes = ApiApplication.class)
	@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
			ModifyDbTest.ModifyTestExecutionListener.class })
	public static class ModifyDbTest extends AbstractTestExecutionListener {

		private static final File UPDATE_EXPECTED_DATA = new File(DATA_DIR + "update_db_expected.xml");
		private static final File DELETED_EXPECTED_DATA = new File(DATA_DIR + "remove_db_expected.xml");

		@Autowired
		private EmployeeRepository target;

		@Autowired
		private DataSourceConfiguration dataSourceConfiguration;

		@Test
		public void testUpdate() throws Exception {
			Employee employee = new Employee();
			employee.setId(1L);
			employee.setName("Taro");

			target.update(employee);
			DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "EMPLOYEE", UPDATE_EXPECTED_DATA,
					//比較対象外
					Arrays.asList("UPDATED_ON"));
		}

		@Test(expected = ResourceNotFoundException.class)
		public void testUpdateException() throws Exception {
			Employee employee = new Employee();
			employee.setId(100L);
			employee.setName("Taro");

			target.update(employee);
			DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "EMPLOYEE", UPDATE_EXPECTED_DATA,
					//比較対象外
					Arrays.asList("UPDATED_ON"));
		}

		@Test
		public void testDelete() throws Exception {
			Employee employee = new Employee();
			employee.setId(1L);
			employee.setDeleted(false);

			target.delete(employee);
			DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "EMPLOYEE", DELETED_EXPECTED_DATA,
					//比較対象外
					Arrays.asList("DELETED_ON"));
		}

		@Test(expected = ResourceNotFoundException.class)
		public void testDeleteException() throws Exception {
			Employee employee = new Employee();
			employee.setId(100L);
			employee.setDeleted(false);

			target.delete(employee);
			DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "EMPLOYEE", DELETED_EXPECTED_DATA,
					//比較対象外
					Arrays.asList("DELETED_ON"));
		}

		/**
		 * テスト前処理、後処理を行なうクラス。 １．各テストメソッドの実行前にDBをテスト用にセットアップする。
		 * ２．各テストメソッドの実行後にDBをテスト実行前の状態に戻す。
		 *
		 * テーブルの更新、削除を複数のケースで検証するため、テストケース毎に上記の前処理、後処理を行なう。
		 */
		static class ModifyTestExecutionListener extends AbstractTestExecutionListener {

			private static File backup;

			/**
			 * テストで利用するテーブルのバックアップを取得する。 テストで利用するデータをloadする。
			 */
			@Override
			public void beforeTestMethod(TestContext testContext) throws Exception {
				DataSource source = getDataSource(testContext.getApplicationContext());
				backup = new File(DATA_DIR + "EMPLOYEE_back.xml");
				DbUnitUtil.backup(source, backup, "EMPLOYEE");
				DbUnitUtil.loadData(source, INIT_DATA);
			}

			/**
			 * 取得したバックアップをリストアし、実行前の状態に復元する。
			 */
			@Override
			public void afterTestMethod(TestContext testContext) throws Exception {
				DbUnitUtil.restoreBackup(getDataSource(testContext.getApplicationContext()), backup);
			}

			private DataSource getDataSource(ApplicationContext applicationContext) throws Exception {
				return applicationContext.getBean(DataSourceConfiguration.class).dataSource();
			}

		}
	}

}
